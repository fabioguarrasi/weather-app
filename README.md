# Weather App

This Retrieves current weather info from selected location and the forecast for the next 3 days.

![Screenshot](screenshot.png)

## Used Techonologies

* [Vuejs](https://vuejs.org/)
* [Vuetify](https://vuetifyjs.com/en/)
* [Vue.observable](https://austincooper.dev/2019/08/09/vue-observable-state-store/)
* [Vue composition API](https://vue-composition-api-rfc.netlify.com/)
* [Openweathermap](https://openweathermap.org/)

## Test Coverage

No test coverage is provided for the moment :cry: