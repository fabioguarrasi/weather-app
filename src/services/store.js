import Vue from 'vue';
import * as weatherAPI from '../services/WeatherApi';

export const state = Vue.observable({
  locations: [
    {
      id: 0,
      name: 'Current Location',
      coord: {
        lon: 0,
        lat: 0,
      }
    },
    {
      id: 2950158,
      name: 'Berlin',
      state: '',
      country: 'DE',
      coord: {
        lon: 10.45,
        lat: 54.033329
      }
    },
    {
      id: 2964574,
      name: 'Dublin',
      state: '',
      country: 'IE',
      coord: {
        lon: -6.26719,
        lat: 53.34399
      }
    },
    {
      id: 3117735,
      name: 'Madrid',
      state: '',
      country: 'ES',
      coord: {
        lon: -3.70256,
        lat: 40.4165
      }
    },
    {
      id: 3169070,
      name: 'Rome',
      state: '',
      country: 'IT',
      coord: {
        lon: 12.4839,
        lat: 41.894741
      }
    },
    {
      id: 6357709,
      name: 'Granada',
      state: '',
      country: 'ES',
      coord: {
        lon: -3.59078,
        lat: 37.188629
      }
    },
    {
      id: 1261481,
      name: 'New Delhi',
      state: '',
      country: 'IN',
      coord: {
        lon: 77.23114,
        lat: 28.61282
      }
    },
  ],
  currentWeather: {},
  forecast: [],
  forecastDays: 3,
  isMetricSystem: true,
  isDrawerOpen: true,
  selectedLocationId: 0,
  MAX_FORECAST_DAYS: 5,
  MOBILE_BREAKPOINT: 960,
});

export const getters = {
  /**
   * passing the filter string (added via input text) it will return an array of location which name contain the filter string.
   * @param {String} filter
   * @retuns {Array<Location>}
   */
  filteredLocations: (filter) => state.locations.filter(location => !filter || location.name.toLowerCase().indexOf(filter.toLowerCase()) > -1),

  /**
   * It returns a subarray of forecast based on the selected number of forecast days.
   * @returns {Array<Object>}
   */
  filteredForecast: () => {
    return state.forecast.length > 0
      ? state.forecast.slice(0, state.forecastDays)
      : [];
  },

  /**
   * It returns the name of the selected location.
   * @retuns {String}
   */
  selectedLocationName: () => {
    return Object.keys(state.currentWeather).length > 0 ? state.currentWeather.name : 'No Location has been selected';
  },

  /**
   * It returns the selected location object.
   * @returns {Object<Location>}
   */
  selectedLocation: () => {
    const selectedLocation = state.locations.filter(location => location.id === state.selectedLocationId);
    return selectedLocation.length > 0 ? selectedLocation[0] : null;
  },

  /**
   * It returns the wind unit based on the selected system
   * @returns {String}
   */
  windLabel() {
    return state.isMetricSystem ? 'mps' : 'mph';
  },

  /**
   * It returns the temperature unit based on the selected system
   * @returns {String}
   */
  temperatureLabel() {
    return state.isMetricSystem ? '°C' : '°F';
  }
};

export const methods = {
  /**
   * It converts the retrieved date in a more UI friendly format.
   * @param {String} stringValue
   * @returns {String}
   */
  getFormattedDate: (stringValue) => {
    const date = new Date(stringValue);
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  },

  /**
   * It retrieves the following info for the selected location:
   * - Current weather
   * - Forecast
   * Once both Info are retrived, they will stored in state
   */
  getWeather: () => {
    const selectedLocation = getters.selectedLocation();
    weatherAPI.getCurrentWeatherByCoordinates(selectedLocation.coord, state.isMetricSystem ? 'metric' : 'imperial')
    .then((currentWeather) => {
      methods.setCurrentWeather({...currentWeather});
    });

    weatherAPI.getWeatherForecastByCoordinates(selectedLocation.coord, state.isMetricSystem ? 'metric' : 'imperial')
    .then((forecast) => {
      if(forecast.cnt > 0) {
        const newForecast = [];
        let counter = 0;
        forecast.list.forEach((item, index) => {
          if(index === 0 || counter >= state.MAX_FORECAST_DAYS) {
            return;
          }
          if(index%8 === 0) {
            newForecast.push({...item});
            counter++;
          }
        });

        /**
         * the forEach makes jumps of 24 hours. if the just doesn't rich the 5th day, t
         * his will be added by the next check
         */
        if(newForecast.length === state.MAX_FORECAST_DAYS-1) {
          newForecast.push({ ...forecast.list[forecast.list.length-1] });
        }
        methods.setForecast(newForecast);
      }
    });
  },

  /**
   * It sets the weather object retrieved from API in the state.
   * @param {Object} weatherObj
   */
  setCurrentWeather: (weatherObj) => {
    state.currentWeather = {...weatherObj};
  },

  setDrawerStatus: (newStatus) => {
    state.isDrawerOpen = newStatus;
  },

  setForecast: (newForecast) => {
    state.forecast = [...newForecast];
  },


  setForecastDay: (newDays) => {
    state.forecastDays = newDays;
  },

  /**
   * After setting the new location, it reloads the current weather and forecast
   * @param {Int} newLocationId. Then Id of the selected location
   */
  setLocation: (newLocationId = 0) => {
    state.selectedLocationId = newLocationId;
    methods.getWeather();
  },

  /**
   * It sets the current user coordanates. The passed objet must contain lat and lon props.
   * @param {Object} coords
   */
  setUserCoordinates: (coords) => {
    if(coords.lat !== null  && coords.lon !== null) {
      state.locations[0].coord = {
        lon: coords.longitude,
        lat: coords.latitude
      };
    }
  },

  /**
   * It toggles between metric and imperial system and re-update the forecast using the new units
   * @param {Boolean} newState
   */
  setTemperatureSystem: (newState) => {
    state.isMetricSystem = newState;
    methods.getWeather();
  },
};