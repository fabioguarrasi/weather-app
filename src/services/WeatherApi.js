import axios from 'axios';

const APP_ID = '56efd173c862baa0e3962edeabeb7721';

/**
 * It retrieves the current weather by passing the location coordinates
 * @param {Object} coords
 * @param {String} units
 * @returns {Promise<Object> || null}
 */
export const getCurrentWeatherByCoordinates = async (coords, units) => {
  try {
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${coords.lat}&lon=${coords.lon}&units=${units}&appid=${APP_ID}`);
    if(response && response.data) {
      return response.data;
    }
    return null;
  } catch(error) {
    return null;
  }
};

/**
 * It retrieves 5 days forecast for the current location by passing the location coordinates
 * @param {Object} coords
 * @param {String} units
 * @returns {Promise<Object> || null}
 */
export const getWeatherForecastByCoordinates = async (coords, units) => {
  try {
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?lat=${coords.lat}&lon=${coords.lon}&units=${units}&appid=${APP_ID}`);
    if(response && response.data) {
      return response.data;
    }
    return null;
  } catch(error) {
    return null;
  }
};